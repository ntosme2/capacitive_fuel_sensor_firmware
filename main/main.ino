
static float MAX_SENSE_VAL = 268435456;
static float OSC_FREQ = 16000000;
static float SINGLE_SCALE = 0.5;
static float L_sense = 1e-3;

#include <Wire.h>
#include "FDC2214.h"
FDC2214 capsense(FDC2214_I2C_ADDR_1); // Use FDC2214_I2C_ADDR_1 

// ###
void setup() {
  
  // ### Start I2C 
  Wire.begin();
  //Wire.setClock(100000L);
  
  // ### Start serial
  Serial.begin(76800);
  Serial.println("\nFDC2x1x test");
  //while(true)
  //    Serial.println("foo");
  
  // ### Start FDC
  // Start FDC2212 with 2 channels init
  //bool capOk = capsense.begin(0x3, 0x4, 0x5, false); //setup first two channels, autoscan with 2 channels, deglitch at 10MHz, external oscillator 
  // Start FDC2214 with 4 channels init
  while(true) {
      bool capOk = capsense.begin(0xF, 0x6, 0x5, false); //setup all four channels, autoscan with 4 channels, deglitch at 10MHz, external oscillator 
      if(capOk)
          break;
      Serial.println("init failed");
  }
  // Start FDC2214 with 4 channels init
  //bool capOk = capsense.begin(0xF, 0x6, 0x5, true); //setup all four channels, autoscan with 4 channels, deglitch at 10MHz, internal oscillator 
  //if (capOk) Serial.println("Sensor OK");  
  //else Serial.println("Sensor Fail");  

}

// ### Tell aplication how many chanels will be smapled in main loop
#define CHAN_COUNT 4
float cap_empty = 1;
float cap_full = 1;
float cap[CHAN_COUNT]; // variable to store data from FDC

// ### 
void loop() {
  
  
  for (int i = 0; i < CHAN_COUNT; i++){ // for each channel
    // ### read 28bit data
    unsigned long raw= capsense.getReading28(i);  
    float Fsensor = raw * OSC_FREQ * SINGLE_SCALE / MAX_SENSE_VAL;
    float tmp = 2 * PI * Fsensor;
    float Csensor = 1 / (L_sense * tmp * tmp);
    cap[i] = Csensor;
    // ### Transmit data to serial in simple format readable by SerialPlot application.
    //Serial.print(Csensor / 1e-12);  
    //if (i < CHAN_COUNT-1) Serial.print(", ");
    //else Serial.println("");
  }
  
  if(Serial.available() > 0) {
    char b = Serial.read();
    if(b == '1')
      cap_full = cap[0];
    if(b == '0')
      cap_empty = cap[0];
  }
  Serial.print(cap[0] / 1e-12);
  Serial.print(", ");
  Serial.print(cap_empty / 1e-12);
  Serial.print(", ");
  Serial.print(cap_full / 1e-12);
  Serial.print(", ");
  Serial.print(100 * (cap[0] - cap_empty) / (cap_full - cap_empty));
  Serial.println("%");
  // No point in sleeping
  //delay(100); 
}
